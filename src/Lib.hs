{-# LANGUAGE FlexibleInstances #-}

module Lib where

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import V2
import Debug.Trace (traceShowId)

data AType = Normal deriving (Show, Eq)

data Asteroid = Asteroid V2fr AType deriving (Show, Eq) -- pos/vel, type

data Player = Player V2fr V2fr -- position, velocity, size

data World = World
  { wAsteroids :: [Asteroid],
    wPlayer :: Player,
    wCanvasSize :: V2f,
    wTime :: Float
  }

asteroidSize = 0.05

initial :: World
initial = World {wAsteroids = [], wPlayer = ip, wCanvasSize = 800 :| 500, wTime = 0}
  where
    ip = Player (0 :| 0, 0 :| 0) (0.1 :| 0.1, 0 :| 0)

draw :: World -> Picture
draw w = Scale c c $ Pictures [player, asteroids, bounds]
  where
    c = (\(x :| y) -> min x y / 2) $ wCanvasSize w
    player = Translate (cX pos) (cY pos) $ Color white $ Scale (cX size) (cY size) $ ThickCircle 1 0.05
    (Player (pos, vel) (size, sizev)) = wPlayer w
    bounds = Color red $ ThickCircle 1 0.01
    asteroids = Pictures $ map drawAsteroid $ wAsteroids w
    drawAsteroid (Asteroid (x :| y, _) t) = Translate x y $ Color yellow $ ThickCircle asteroidSize (asteroidSize / 10)

step :: Float -> World -> World
step d w =
  World
    { wAsteroids = take 100 (oldAsteroids ++ [newAsteroid]),
      wPlayer = wPlayer w,
      wCanvasSize = wCanvasSize w,
      wTime = d + wTime w
    }
  where
    oldAsteroids = filter shouldDespawn $ map stepAsteroid $ wAsteroids w
    shouldDespawn (Asteroid (pos, _) t) = lengthV2 pos < 1.5
    newAsteroid = Asteroid (randomTM 1 * 0.5, randomTM 1.52314 * 0.1) Normal
    randomTM seed = sin (wTime w * 1234 * seed) :| cos (wTime w * 1234 * seed)
    stepAsteroid = stepAsteroidCollision . stepAsteroidVelocity
    stepAsteroidVelocity (Asteroid (pos, vel) t) = Asteroid (pos + vel * pure d, vel) t
    stepAsteroidCollision this = foldr collide this colliders
      where
        astsDoCollide (Asteroid (ap, _) _) (Asteroid (bp, _) _) = lengthV2 (ap - bp) < asteroidSize * 2
        colliders = filter (astsDoCollide this) $ filter (/= this) $ wAsteroids w
        collide (Asteroid (opos, ovel) ot) (Asteroid (tpos, tvel) tt) = Asteroid (tpos, ovel) tt
          where
            normal = normalizeV2 (opos - tpos)


changeSizeVel :: V2f -> World -> World
changeSizeVel v w = w { wPlayer = (\(Player p (s, sv)) -> Player p (s, sv + v)) $ wPlayer w }

input :: Event -> World -> World
input (EventKey (Char 'a') Up mods _) w = changeSizeVel (1 :| 0) w
input (EventKey (Char 'a') Down mods _) w = changeSizeVel ((-1) :| 0) w
input (EventKey (Char 'd') Up mods _) w = changeSizeVel ((-1) :| 0) w
input (EventKey (Char 'd') Down mods _) w = changeSizeVel (1 :| 0) w
input (EventResize (x, y)) w = w {wCanvasSize = fmap (fromInteger . toInteger) $ x :| y}
input _ x = x

libMain :: IO ()
libMain = play (InWindow "ld49" (800, 500) (0, 0)) black 60 initial draw input step
