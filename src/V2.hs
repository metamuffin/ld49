{-# LANGUAGE FlexibleInstances #-}
module V2 where

import GHC.Base (liftA2)

data V2 a = a :| a deriving Eq
type V2f = V2 Float
type V2fr = (V2f, V2f) -- value, rate of change

instance Functor V2 where
  fmap f (x :| y) = f x :| f y
instance Applicative V2 where
  pure x = x :| x
  liftA2 f (ax :| ay) (bx :| by) = f ax bx :| f ay by
  
instance Num V2f where
  (+) = liftA2 (+)
  (*) = liftA2 (*)
  (-) = liftA2 (-)
  abs = fmap abs
  signum = fmap signum
  fromInteger = (:| 0.0) . fromInteger

instance Fractional V2f where
  fromRational n = fmap fromRational $ n :| n
  (/) = liftA2 (/)
  
instance Show a => Show (V2 a) where
  show (x :| y) = "(" ++ show x ++ "|" ++ show y ++ ")" 

cX (x :| _) = x
cY (_ :| y) = y

lengthV2 :: Floating a => V2 a -> a
lengthV2 (x :| y) = sqrt (x ** 2) + (y ** 2) 

normalizeV2 :: V2f -> V2f
normalizeV2 v = v / pure (lengthV2 v)

dot :: V2f -> V2f -> Float
dot (ax :| ay) (bx :| by) = ax*bx+ay*by

reflect :: V2f -> V2f -> V2f
reflect d n = d - 2 * n * pure (d `dot` n)

